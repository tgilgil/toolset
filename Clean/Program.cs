﻿using System.IO;
using Core;
using log4net;
using log4net.Config;

namespace Clean
{
    internal class Program : SolutionProgram
    {
        private static ILog Log { get; } = LogManager.GetLogger(typeof(Program));

        private static bool Packages { get; set; } = true;

        static Program()
        {
            XmlConfigurator.Configure();

            Arguments.Add("packages", input =>
                     {
                         bool value;
                         if (bool.TryParse(input, out value)) Packages = value;
                     });
        }

        private static void Main(string[] args)
        {
            Initialize(args, Log);

            if (Packages)
            {
                Log.Info($"Solution [{Solution.FullName}]");
                Delete(Solution.Directory.GetDirectory("packages"));
            }

            foreach (var project in Projects)
            {
                Log.Info($"[{project.Name}]");

                var directory = project.Directory;

                Delete(directory.GetDirectory("bin"));
                Delete(directory.GetDirectory("obj"));
            }

            Log.Info("Finished cleaning solution!");
        }

        private static void Delete(DirectoryInfo directory)
        {
            if (directory.TryDelete())
            {
                Log.Info($"  {directory.Name}/ - Deleted");
            }
            else if (directory.Exists)
            {
                Log.Error($"  {directory.Name}/ - Could not delete!");
            }
        }
    }
}
