﻿using System.Reflection;
using System.Runtime.InteropServices;
using CommandLine;

[assembly: AssemblyTitle("Service Control Power Tool")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Service Control Power Tool")]
[assembly: AssemblyCopyright("Copyright © 2016 Nathaniel Piché")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyLicense("This is free software. You may redistribute copies of it under the terms of",
                           "the MIT License <http://www.opensource.org/licenses/mit-license.php>.")]

[assembly: AssemblyUsage("\nUsage: scpt -q {QUERY} [-c {COMMANDS}] [-s {STATES}] [-t {TIMEOUT}] [-lkdv]",
                         "       scpt -q SLP\\. -s stopped:paused -l -c start",
                         "       scpt -q SLP\\. -vlkd -c stop:uninstall")]

[assembly: ComVisible(false)]
[assembly: Guid("b2d00520-d889-4453-a3fe-5a7636715d97")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.*")]
