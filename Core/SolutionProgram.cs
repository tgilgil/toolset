﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Core.Model;
using log4net;

namespace Core
{
    public class SolutionProgram
    {
        protected static ArgumentsManager Arguments { get; } = new ArgumentsManager();

        protected static string ArgSolution { get; private set; }
        protected static List<string> ArgWhiteList { get; private set; }

        protected static FileInfo Solution { get; private set; }
        protected static List<Project> Projects { get; private set; }

        static SolutionProgram()
        {
            Arguments.Add("solution", solution => ArgSolution = solution);
            Arguments.Add("whitelist", whitelist => ArgWhiteList = whitelist.Split(',').ToList());
        }

        protected static void Initialize(string[] args, ILog log)
        {
            Arguments.Manage(args);

            if (string.IsNullOrWhiteSpace(ArgSolution))
            {
                log.Warn(@"Expecting a solution argument");
                log.Warn(@"    example: solution=c:\dev\test\test.sln");

                Environment.Exit(-1);
            }

            Solution = new FileInfo(Path.GetFullPath(ArgSolution));

            if (!Solution.Exists)
            {
                log.Error($"Solution '{ArgSolution}' does not exists!");
                Environment.Exit(-2);
            }

            Projects = ProjectsLoader.FromSolution(Solution);

            if (ArgWhiteList != null)
            {
                Projects = Projects.Where(p => ArgWhiteList.Contains(p.Name)).ToList();
            }
        }
    }
}
