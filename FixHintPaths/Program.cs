﻿using Core;
using log4net;
using log4net.Config;

namespace FixHintPaths
{
    internal class Program : SolutionProgram
    {
        private static ILog Log { get; } = LogManager.GetLogger(typeof(Program));

        static Program()
        {
            XmlConfigurator.Configure();
        }

        private static void Main(string[] args)
        {
            Initialize(args, Log);

            foreach (var project in Projects)
            {
                project.FixHintPathsPrefixes(Solution.Directory, Log);
            }
        }
    }
}
